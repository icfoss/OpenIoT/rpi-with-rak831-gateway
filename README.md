# LoRaWAN Gateway using RAK831 and Raspberry Pi

The repository contains resources to setup a LoRaWAN Gateway using RPi and RAK831 Concentrator Module.

## Getting Started


* Setup Pi - Enable SSH, SPI, Set time, Keyboard and connect to internet.
* Connect RAK831 to RPi as per the Wire Connection Diagram.
* Power up RPi and Connect to it via [SSH](https://www.raspberrypi.org/documentation/remote-access/ssh/)
* Install git 

```
sudo apt install git
```
* Clone Gateway Application Repository

```
git clone -b spi https://github.com/ttn-zh/ic880a-gateway.git
```
* Install Application

```
cd ic880a-gateway

sudo ./install.sh spi
```
* Change **global_conf.json** in the directory **/opt/ttn-gateway/bin** to suit your regional parameters. Sample parameters can be found [here](https://github.com/TheThingsNetwork/gateway-conf)
Note : Sample configuration for India (IN865) can be found [here](https://gitlab.com/icfoss/OpenIoT/rpi-with-rak831-gateway)

* Restart TTN

```
sudo service ttn-gateway restart
```

* Status can be checked

```
sudo service ttn-gateway status	
```


* Live packet log can be seen using

```
sudo tail -f /var/log/syslog
```

### LoRaServer Setup

To be updated

### Prerequisites

* [RAK831 Board](https://store.rakwireless.com/products/rak831-gateway-module)
  Note : EU868 Version support IN865 band as well
* Raspberry Pi running latest OS*.
* M-M Jumper Wires ( 7 Nos) to make hardware connections.
* Ethernet cable, if WiFi is unavailable.

*Tested to work with Jessie, Stretch and buster.



## Wire Connection

See [Connection Diagram](RAK831+RPI.png).

.........................................

|  RPI          | RAK831        | Desc  |
|:--------------|:-------------:| :----:|
| 2             | 2             | 5V    |
| 6             | 3             | GND   |
| 22 (GPIO25)   | 19            | Reset |
| 23            | 18            | SCK   |
| 21            | 17            | MISO  |
| 19            | 16            | MOSI  |
| 24 (CE0)      | 15            | CSN   |

.........................................

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details





